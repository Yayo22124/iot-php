<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/menu/styles.css">

    <title>Menu | Eli Haziel Ortiz Ramirez</title>
</head>

<body>
    <div class="sidenav">
        <a href="#">About</a>
        <a href="#">Services</a>
        <a href="#">Clients</a>
        <a href="#">Contact</a>
    </div>

    <div class="main">
        <h2>Sidenav Example</h2>
        <p>This sidenav is always shown.</p>
    </div>
</body>

</html>